import { Component } from "react";

class ComponentTime extends Component {
    constructor(props) {
        super(props)
        this.state = { time: [] }
    }
    index= 1;
    updateTime = () => {
        let date = new Date
        this.setState({
            time: [...this.state.time, <p>{this.index}. {date.getHours() % 12} :
                                        {date.getMinutes()} :
                                        {date.getSeconds() > 9 ? date.getSeconds() : "0" + date.getSeconds()}
                                        {date.getHours() > 12 ? " PM" : " AM"}</p>]
        })
        this.index++;
    }
    render() {
        return (
            <div>
                {this.state.time}
                <button onClick={this.updateTime}>Add Time to list</button>
                
            </div>
        )
    }
}
export default ComponentTime